#include "lexer.h"

Token Lexer::getNextToken() {
    std::string input = getText();
    int tam = input.size();
    char str[tam] = input;
        for(int i = 0; i < str.size(); i++){
            if(i == str.size(){
                if(str[i]=='h' || str[i] == 'H'){
                    return Token::Hex;
                }
                else if(isdigit(str[i])){
                    return Token::Decimal;
                }
                else if(str[i]== 'b' || str[i] == 'B'){
                    return Token::Binary;
                }
                else if(str[i] == 'o' || str[i] == 'O'){
                    return Token::Octal;
                }      
            }
        }

    return Token::Eof;
}